export const ssr = false;
export const prerender = false;

// Rename the output filename to something different.For example, you can rename it to "layout.generated.js"

// As file named +layout.js cannot be written because it would overwrite an input file.

// In general, it's best to avoid using special characters like + in filenames.
