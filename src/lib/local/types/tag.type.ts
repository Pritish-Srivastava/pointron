export type Tag = {
    id: string;
    label: string;
    hue?: number;
}