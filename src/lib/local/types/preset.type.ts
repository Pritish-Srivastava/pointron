import type { TimeUnit } from "../../tidy/types/timeUnit.enum"

export type Preset = {
    id: string
    name?: string
    rounds: number
    duration: number
    brek: number
    additional?: Preset[]
    units?: TimeUnit
}